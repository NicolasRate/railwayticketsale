package com.nrate.railwayticketsale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RailwayTicketSaleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RailwayTicketSaleApplication.class, args);
    }

}
