package com.nrate.railwayticketsale.entities;

import javax.persistence.*;

@Entity(name = PlacesType.ENTITY_NAME)
public class PlacesType {
    public static final String ENTITY_NAME = "Places_Type";
    public static final String COLUMN_ID_NAME = "places_type_id";
    public static final String COLUMN_TITLE_NAME = "title";
    public static final String COLUMN_LETTER_NAME = "letter";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_TITLE_NAME, nullable = false, length = 50)
    private String title;

    @Column(name = COLUMN_LETTER_NAME, length = 5)
    private String letter;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

}