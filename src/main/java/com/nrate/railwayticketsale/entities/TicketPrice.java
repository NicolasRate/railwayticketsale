package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = TicketPrice.ENTITY_NAME)
public class TicketPrice {
    public static final String ENTITY_NAME = "Ticket_Price";
    public static final String COLUMN_PRICE_NAME = "price";

    @EmbeddedId
    private TicketPriceId id;

    @MapsId("placesTypeId")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "places_type_id", nullable = false)
    private PlacesType placesType;

    @Column(name = COLUMN_PRICE_NAME, nullable = false, precision = 19, scale = 4)
    private BigDecimal price;

    @MapsId("trainId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "train_id", nullable = false)
    @JsonBackReference("train-price")
    private Train train;

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public TicketPriceId getId() {
        return id;
    }

    public void setId(TicketPriceId id) {
        this.id = id;
    }

    public PlacesType getPlacesType() {
        return placesType;
    }

    public void setPlacesType(PlacesType placesType) {
        this.placesType = placesType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}