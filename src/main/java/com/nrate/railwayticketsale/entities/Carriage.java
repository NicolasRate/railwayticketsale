package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity(name = Carriage.ENTITY_NAME)
public class Carriage {
    public static final String ENTITY_NAME = "Carriage";
    public static final String COLUMN_ID_NAME = "carriage_id";
    public static final String COLUMN_FREEPLACESCOUNT_NAME = "free_places_count";
    public static final String COLUMN_CARRIAGENUMBER_NAME = "carriage_number";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_FREEPLACESCOUNT_NAME)
    private Integer freePlacesCount;

    @Column(name = COLUMN_CARRIAGENUMBER_NAME, nullable = false)
    private Integer carriageNumber;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "carriage_info_id", nullable = false)
//    @JsonBackReference
    private CarriageInfo carriageInfo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "train_id", nullable = false)
    @JsonBackReference(value = "carriage-train")
    private Train train;

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFreePlacesCount() {
        return freePlacesCount;
    }

    public void setFreePlacesCount(Integer freePlacesCount) {
        this.freePlacesCount = freePlacesCount;
    }

    public Integer getCarriageNumber() {
        return carriageNumber;
    }

    public void setCarriageNumber(Integer carriageNumber) {
        this.carriageNumber = carriageNumber;
    }

    public CarriageInfo getCarriageInfo() {
        return carriageInfo;
    }

    public void setCarriageInfo(CarriageInfo carriageInfo) {
        this.carriageInfo = carriageInfo;
    }

}