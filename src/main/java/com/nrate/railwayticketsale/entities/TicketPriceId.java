package com.nrate.railwayticketsale.entities;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TicketPriceId implements Serializable {
    public static final String COLUMN_PLACESTYPEID_NAME = "places_type_id";
    public static final String COLUMN_TRAINID_NAME = "train_id";
    private static final long serialVersionUID = 2972510396444404010L;

    @Column(name = COLUMN_PLACESTYPEID_NAME, nullable = false)
    private Integer placesTypeId;

    @Column(name = COLUMN_TRAINID_NAME, nullable = false)
    private Integer trainId;

    public Integer getPlacesTypeId() {
        return placesTypeId;
    }

    public void setPlacesTypeId(Integer placesTypeId) {
        this.placesTypeId = placesTypeId;
    }

    public Integer getTrainId() {
        return trainId;
    }

    public void setTrainId(Integer trainId) {
        this.trainId = trainId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TicketPriceId entity = (TicketPriceId) o;
        return Objects.equals(this.trainId, entity.trainId) &&
                Objects.equals(this.placesTypeId, entity.placesTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainId, placesTypeId);
    }

}