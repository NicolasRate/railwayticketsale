package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity(name = TicketAdditionalService.ENTITY_NAME)
public class TicketAdditionalService {
    public static final String ENTITY_NAME = "Ticket_Additional_Service";

    @EmbeddedId
    private TicketAdditionalServiceId id;

    @MapsId("ticketId")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ticket_id", nullable = false)
    @JsonBackReference("ticket-additionalServices")
    private Ticket ticket;

    @MapsId("additionalServiceId")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "additional_service_id", nullable = false)
    private AdditionalService additionalService;

    public AdditionalService getAdditionalService() {
        return additionalService;
    }

    public void setAdditionalService(AdditionalService additionalService) {
        this.additionalService = additionalService;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public TicketAdditionalServiceId getId() {
        return id;
    }

    public void setId(TicketAdditionalServiceId id) {
        this.id = id;
    }

//TODO [JPA Buddy] generate columns from DB
}