package com.nrate.railwayticketsale.entities;

import javax.persistence.*;

@Entity(name = Cashier.ENTITY_NAME)
public class Cashier {
    public static final String ENTITY_NAME = "Cashier";
    public static final String COLUMN_ID_NAME = "cashier_id";
    public static final String COLUMN_FIRSTNAME_NAME = "first_name";
    public static final String COLUMN_LASTNAME_NAME = "last_name";
    public static final String COLUMN_EMAIL_NAME = "email";
    public static final String COLUMN_PHONENUMBER_NAME = "phone_number";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_FIRSTNAME_NAME, nullable = false, length = 50)
    private String firstName;

    @Column(name = COLUMN_LASTNAME_NAME, nullable = false, length = 50)
    private String lastName;

    @Column(name = COLUMN_EMAIL_NAME, nullable = false, length = 50)
    private String email;

    @Column(name = COLUMN_PHONENUMBER_NAME, nullable = false, length = 50)
    private String phoneNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}