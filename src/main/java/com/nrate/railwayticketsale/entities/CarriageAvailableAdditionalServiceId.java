package com.nrate.railwayticketsale.entities;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CarriageAvailableAdditionalServiceId implements Serializable {
    public static final String COLUMN_CARRIAGEINFOID_NAME = "carriage_info_id";
    public static final String COLUMN_ADDITIONALSERVICEID_NAME = "additional_service_id";
    private static final long serialVersionUID = -1343936994935472929L;

    @Column(name = COLUMN_CARRIAGEINFOID_NAME, nullable = false)
    private Integer carriageInfoId;

    @Column(name = COLUMN_ADDITIONALSERVICEID_NAME, nullable = false)
    private Integer additionalServiceId;

    public Integer getCarriageInfoId() {
        return carriageInfoId;
    }

    public void setCarriageInfoId(Integer carriageInfoId) {
        this.carriageInfoId = carriageInfoId;
    }

    public Integer getAdditionalServiceId() {
        return additionalServiceId;
    }

    public void setAdditionalServiceId(Integer additionalServiceId) {
        this.additionalServiceId = additionalServiceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        CarriageAvailableAdditionalServiceId entity = (CarriageAvailableAdditionalServiceId) o;
        return Objects.equals(this.carriageInfoId, entity.carriageInfoId) &&
                Objects.equals(this.additionalServiceId, entity.additionalServiceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carriageInfoId, additionalServiceId);
    }

}