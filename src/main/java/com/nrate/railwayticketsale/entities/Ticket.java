package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.Hibernate;
import org.springframework.data.repository.query.Param;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLType;
import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity(name = Ticket.ENTITY_NAME)
@NamedStoredProcedureQuery(
        name="CreateTicket",
        procedureName="CreateTicket",
        resultClasses = { Ticket.class },
        parameters={
                @StoredProcedureParameter(name="passenger_id", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="cashier_id", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="dep_station_id", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="arr_station_id", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="train_id", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="carriage_id", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="places_type_id", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="place_number", type = Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="price", type = BigDecimal.class, mode=ParameterMode.IN)
        }
)
public class Ticket {
    public static final String ENTITY_NAME = "Ticket";
    public static final String COLUMN_ID_NAME = "ticket_id";
    public static final String COLUMN_PRICE_NAME = "price";
    public static final String COLUMN_SALETIME_NAME = "sale_time";
    public static final String COLUMN_PLACENUMBER_NAME = "place_number";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "cashier_id", nullable = false)
//    @JsonBackReference
    private Cashier cashier;

    @Column(name = COLUMN_PRICE_NAME, nullable = false, precision = 19, scale = 4)
    private BigDecimal price;

    @Column(name = COLUMN_SALETIME_NAME, nullable = true)
    private Instant saleTime;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "carriage_id", nullable = false)
//    @JsonBackReference(value = "carriage")
    private Carriage carriage;

    @Column(name = COLUMN_PLACENUMBER_NAME, nullable = false)
    private Integer placeNumber;

    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "passenger_id", nullable = false)
    @JsonBackReference
    private Passenger passenger;

    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name = "train_id", referencedColumnName = "train_id", updatable = false, insertable = false),
            @JoinColumn(name = "dep_station_id", referencedColumnName = "station_id", nullable = false, updatable = false, insertable = false)
    })
//    @JsonBackReference
    private StationInRoute depStation;

    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name = "train_id", referencedColumnName = "train_id", updatable = false, insertable = false),
            @JoinColumn(name = "arr_station_id", referencedColumnName = "station_id", nullable = false, updatable = false, insertable = false)
    })
//    @JsonBackReference
    private StationInRoute arrStation;

    @OneToMany(mappedBy = "ticket")
    @JsonManagedReference(value = "ticket-additionalServices")
    @ToString.Exclude
    private Set<TicketAdditionalService> additionalServices = new LinkedHashSet<>();

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "train_id", nullable = false, updatable = false, insertable = false)
    private Train train;

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Ticket ticket = (Ticket) o;
        return id != null && Objects.equals(id, ticket.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cashier getCashier() {
        return cashier;
    }

    public void setCashier(Cashier cashier) {
        this.cashier = cashier;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Instant getSaleTime() {
        return saleTime;
    }

    public void setSaleTime(Instant saleTime) {
        this.saleTime = saleTime;
    }

    public Carriage getCarriage() {
        return carriage;
    }

    public void setCarriage(Carriage carriage) {
        this.carriage = carriage;
    }

    public Integer getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(Integer placeNumber) {
        this.placeNumber = placeNumber;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public StationInRoute getDepStation() {
        return depStation;
    }

    public void setDepStation(StationInRoute depStation) {
        this.depStation = depStation;
    }

    public StationInRoute getArrStation() {
        return arrStation;
    }

    public void setArrStation(StationInRoute arrStation) {
        this.arrStation = arrStation;
    }

    public Set<TicketAdditionalService> getAdditionalServices() {
        return additionalServices;
    }

    public void setAdditionalServices(Set<TicketAdditionalService> additionalServices) {
        this.additionalServices = additionalServices;
    }
}