package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = CarriageAvailableAdditionalService.ENTITY_NAME)
public class CarriageAvailableAdditionalService {
    public static final String ENTITY_NAME = "Carriage_Available_Additional_Service";
    public static final String COLUMN_PRICE_NAME = "price";

    @EmbeddedId
    private CarriageAvailableAdditionalServiceId id;

    @MapsId("carriageInfoId")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "carriage_info_id", nullable = false)
//    @JsonIgnoreProperties("availableAdditionalServices")
    @JsonBackReference("carriageInfo-service")
    private CarriageInfo carriageInfo;

    @MapsId("additionalServiceId")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "additional_service_id", nullable = false)
    @JsonBackReference("additional-service-serivce")
    private AdditionalService additionalService;

    @Column(name = COLUMN_PRICE_NAME, nullable = false, precision = 19, scale = 4)
    private BigDecimal price;

    public CarriageAvailableAdditionalServiceId getId() {
        return id;
    }

    public void setId(CarriageAvailableAdditionalServiceId id) {
        this.id = id;
    }

    public CarriageInfo getCarriageInfo() {
        return carriageInfo;
    }

    public void setCarriageInfo(CarriageInfo carriageInfo) {
        this.carriageInfo = carriageInfo;
    }

    public AdditionalService getAdditionalService() {
        return additionalService;
    }

    public void setAdditionalService(AdditionalService additionalService) {
        this.additionalService = additionalService;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}