package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity(name = StationInRoute.ENTITY_NAME)
@NoArgsConstructor
public class StationInRoute implements Serializable {
    public static final String ENTITY_NAME = "Station_In_Route";
    public static final String COLUMN_DISTANCEFROMSTART_NAME = "distance_from_start";
    public static final String COLUMN_ORDERNUMBER_NAME = "order_number";
    public static final String COLUMN_ARRTIME_NAME = "arr_time";
    public static final String COLUMN_DEPTIME_NAME = "dep_time";
    public static final String COLUMN_DEPDATE_NAME = "dep_date";
    public static final String COLUMN_ARRDATE_NAME = "arr_date";


    @EmbeddedId
    private StationInRouteId id;

    @MapsId("stationId")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "station_id", nullable = false)
    private Station station;

    @Column(name = COLUMN_DISTANCEFROMSTART_NAME)
    private Integer distanceFromStart;

    @Column(name = COLUMN_ORDERNUMBER_NAME)
    private Integer orderNumber;

    @Column(name = COLUMN_ARRTIME_NAME)
    private LocalTime arrTime;

    @Column(name = COLUMN_DEPTIME_NAME)
    private LocalTime depTime;

    @Column(name = COLUMN_DEPDATE_NAME)
    private LocalDate depDate;

    @Column(name = COLUMN_ARRDATE_NAME)
    private LocalDate arrDate;

    @MapsId("trainId")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "train_id", nullable = false)
    @JsonBackReference
    private Train train;






}