package com.nrate.railwayticketsale.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class StationInRouteId implements Serializable {
    public static final String COLUMN_TRAINID_NAME = "train_id";
    public static final String COLUMN_STATIONID_NAME = "station_id";
    private static final long serialVersionUID = 2127606165981324090L;

    @Column(name = COLUMN_TRAINID_NAME, nullable = false)
    private Integer trainId;

    @Column(name = COLUMN_STATIONID_NAME, nullable = false)
    private Integer stationId;

    public Integer getTrainId() {
        return trainId;
    }

    public void setTrainId(Integer trainId) {
        this.trainId = trainId;
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StationInRouteId entity = (StationInRouteId) o;
        return Objects.equals(this.trainId, entity.trainId) &&
                Objects.equals(this.stationId, entity.stationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainId, stationId);
    }

}