package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@Entity(name = Passenger.ENTITY_NAME)
@NoArgsConstructor
public class Passenger {
    public static final String ENTITY_NAME = "Passenger";
    public static final String COLUMN_ID_NAME = "passenger_id";
    public static final String COLUMN_FIRSTNAME_NAME = "first_name";
    public static final String COLUMN_LASTNAME_NAME = "last_name";
    public static final String COLUMN_AGE_NAME = "age";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_FIRSTNAME_NAME, length = 50)
    private String firstName;

    @Column(name = COLUMN_LASTNAME_NAME, length = 50)
    private String lastName;

    @Column(name = COLUMN_AGE_NAME, nullable = false)
    private Integer age;

    @OneToMany(mappedBy = "passenger")
    @JsonManagedReference
    private Set<Ticket> tickets = new LinkedHashSet<>();

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}