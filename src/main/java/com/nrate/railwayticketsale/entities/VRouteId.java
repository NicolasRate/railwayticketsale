package com.nrate.railwayticketsale.entities;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class VRouteId implements Serializable {
    public static final String COLUMN_TRAINID_NAME = "train_id";
    public static final String COLUMN_STATIONID_NAME = "station_id";
    private static final long serialVersionUID = 7879109652264186581L;

    @Column(name = COLUMN_TRAINID_NAME, nullable = false)
    private Integer trainId;

    @Column(name = COLUMN_STATIONID_NAME, nullable = false)
    private Integer stationId;

    public Integer getTrainId() {
        return trainId;
    }

    public Integer getStationId() {
        return stationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        VRouteId entity = (VRouteId) o;
        return Objects.equals(this.trainId, entity.trainId) &&
                Objects.equals(this.stationId, entity.stationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainId, stationId);
    }

}