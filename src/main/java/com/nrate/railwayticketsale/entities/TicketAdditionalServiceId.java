package com.nrate.railwayticketsale.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class TicketAdditionalServiceId implements Serializable {
    public static final String COLUMN_TICKETID_NAME = "ticket_id";
    public static final String COLUMN_ADDITIONALSERVICEID_NAME = "additional_service_id";
    private static final long serialVersionUID = -8701754229363879973L;

    @Column(name = COLUMN_TICKETID_NAME, nullable = false)
    private Integer ticketId;

    @Column(name = COLUMN_ADDITIONALSERVICEID_NAME, nullable = false)
    private Integer additionalServiceId;

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Integer getAdditionalServiceId() {
        return additionalServiceId;
    }

    public void setAdditionalServiceId(Integer additionalServiceId) {
        this.additionalServiceId = additionalServiceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TicketAdditionalServiceId entity = (TicketAdditionalServiceId) o;
        return Objects.equals(this.additionalServiceId, entity.additionalServiceId) &&
                Objects.equals(this.ticketId, entity.ticketId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(additionalServiceId, ticketId);
    }

}