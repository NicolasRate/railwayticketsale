package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Data
@NoArgsConstructor
@Entity(name = Train.ENTITY_NAME)
@NamedStoredProcedureQuery(
        name="GetTrainByStationsAndDate",
        procedureName="GetTrainByStationsAndDate",
        resultClasses = { Train.class },
        parameters={
                @StoredProcedureParameter(name="dep_station_id", type=Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="arr_station_id", type=Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="dep_date", type=String.class, mode=ParameterMode.IN)
        }
)
@NamedStoredProcedureQuery(
        name="GetTicketPrices",
        procedureName="GetTicketPrices",
        parameters={
                @StoredProcedureParameter(name="dep_station_id", type=Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="arr_station_id", type=Integer.class, mode=ParameterMode.IN),
                @StoredProcedureParameter(name="dep_date", type=String.class, mode=ParameterMode.IN)
        }
)
public class Train {
    public static final String ENTITY_NAME = "Train";
    public static final String COLUMN_ID_NAME = "train_id";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_DISTANCE_NAME = "distance";
    public static final String COLUMN_DEPDATE_NAME = "dep_date";
    public static final String COLUMN_DEPTIME_NAME = "dep_time";
    public static final String COLUMN_ARRDATE_NAME = "arr_date";
    public static final String COLUMN_ARRTIME_NAME = "arr_time";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 50)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "start_station_id", nullable = false)
    private Station startStation;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "end_station_id", nullable = false)
    private Station endStation;

    @Column(name = COLUMN_DISTANCE_NAME)
    private Integer distance;

    @Column(name = COLUMN_DEPDATE_NAME)
    private LocalDate depDate;

    @Column(name = COLUMN_DEPTIME_NAME)
    private LocalTime depTime;

    @Column(name = COLUMN_ARRDATE_NAME)
    private LocalDate arrDate;

    @Column(name = COLUMN_ARRTIME_NAME)
    private LocalTime arrTime;

    @OneToMany(mappedBy = "train")
    @JsonManagedReference("carriage-train")
    private Set<Carriage> carriages = new LinkedHashSet<>();

    @OneToMany(mappedBy = "train")
    @JsonManagedReference("train-price")
    private Set<TicketPrice> ticketPrices = new LinkedHashSet<>();

    public Set<TicketPrice> getTicketPrices() {
        return ticketPrices;
    }

    public void setTicketPrices(Set<TicketPrice> ticketPrices) {
        this.ticketPrices = ticketPrices;
    }

    public Set<Carriage> getCarriages() {
        return carriages;
    }

    public void setCarriages(Set<Carriage> carriages) {
        this.carriages = carriages;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Station getStartStation() {
        return startStation;
    }

    public void setStartStation(Station startStation) {
        this.startStation = startStation;
    }

    public Station getEndStation() {
        return endStation;
    }

    public void setEndStation(Station endStation) {
        this.endStation = endStation;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public LocalDate getDepDate() {
        return depDate;
    }

    public void setDepDate(LocalDate depDate) {
        this.depDate = depDate;
    }

    public LocalTime getDepTime() {
        return depTime;
    }

    public void setDepTime(LocalTime depTime) {
        this.depTime = depTime;
    }

    public LocalDate getArrDate() {
        return arrDate;
    }

    public void setArrDate(LocalDate arrDate) {
        this.arrDate = arrDate;
    }

    public LocalTime getArrTime() {
        return arrTime;
    }

    public void setArrTime(LocalTime arrTime) {
        this.arrTime = arrTime;
    }

}