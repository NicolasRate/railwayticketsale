package com.nrate.railwayticketsale.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = Railway.ENTITY_NAME)
public class Railway {
    public static final String ENTITY_NAME = "Railway";
    public static final String COLUMN_ID_NAME = "railway_id";
    public static final String COLUMN_NAME_NAME = "name";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_NAME_NAME, length = 50)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}