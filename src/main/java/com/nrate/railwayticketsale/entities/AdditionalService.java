package com.nrate.railwayticketsale.entities;

import javax.persistence.*;

@Entity(name = AdditionalService.ENTITY_NAME)
public class AdditionalService {
    public static final String ENTITY_NAME = "Additional_Service";
    public static final String COLUMN_ID_NAME = "additional_service_id";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_DESCRIPTION_NAME = "description";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 50)
    private String name;

    @Column(name = COLUMN_DESCRIPTION_NAME, length = 300)
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}