package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity(name = VCarriageAdditionalService.ENTITY_NAME)
@Immutable
@Table(name = VCarriageAdditionalService.TABLE_NAME)
public class VCarriageAdditionalService {
    public static final String ENTITY_NAME = "V_Carriage_Additional_Service";
    public static final String TABLE_NAME = "v_carriage_additional_services";
    public static final String COLUMN_CARRIAGENUMBER_NAME = "carriage_number";
    public static final String COLUMN_PRICE_NAME = "price";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_DESCRIPTION_NAME = "description";
    public static final String COLUMN_CARRIAGE_ID = "carriage_id";
    public static final String COLUMN_ADDITIONAL_SERVICE_ID = "additional_service_id";

    @EmbeddedId
    @JsonIgnore
    private VCarriageAdditionalServiceId id;


    @Column(name = COLUMN_CARRIAGE_ID, nullable = false, updatable = false, insertable = false)
    private Integer carriageId;

    @Column(name = COLUMN_ADDITIONAL_SERVICE_ID, nullable = false, updatable = false, insertable = false)
    private Integer additionalServiceId;

    @Column(name = COLUMN_CARRIAGENUMBER_NAME, nullable = false)
    private Integer carriageNumber;

    @Column(name = COLUMN_PRICE_NAME, nullable = false, precision = 19, scale = 4)
    private BigDecimal price;

    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 50)
    private String name;

    @Column(name = COLUMN_DESCRIPTION_NAME, length = 300)
    private String description;

    public VCarriageAdditionalServiceId getId() {
        return id;
    }

    public void setId(VCarriageAdditionalServiceId id) {
        this.id = id;
    }

    public Integer getCarriageNumber() {
        return carriageNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getCarriageId() {
        return carriageId;
    }

    public void setCarriageId(Integer carriageId) {
        this.carriageId = carriageId;
    }

    public Integer getAdditionalServiceId() {
        return additionalServiceId;
    }

    public void setAdditionalServiceId(Integer additionalServiceId) {
        this.additionalServiceId = additionalServiceId;
    }

    protected VCarriageAdditionalService() {
    }
}