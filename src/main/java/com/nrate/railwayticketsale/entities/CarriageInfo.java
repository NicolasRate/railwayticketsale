package com.nrate.railwayticketsale.entities;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity(name = CarriageInfo.ENTITY_NAME)
public class CarriageInfo {
    public static final String ENTITY_NAME = "Carriage_Info";
    public static final String COLUMN_ID_NAME = "carriage_info_id";
    public static final String COLUMN_PLACESCOUNT_NAME = "places_count";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    private Integer id;

    @Column(name = COLUMN_PLACESCOUNT_NAME, nullable = false)
    private Integer placesCount;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "places_type_id", nullable = false)
    private PlacesType placesType;

    @OneToMany(mappedBy = "carriageInfo")
    private Set<CarriageAvailableAdditionalService> availableAdditionalServices = new LinkedHashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlacesCount() {
        return placesCount;
    }

    public void setPlacesCount(Integer placesCount) {
        this.placesCount = placesCount;
    }

    public PlacesType getPlacesType() {
        return placesType;
    }

    public void setPlacesType(PlacesType placesType) {
        this.placesType = placesType;
    }

    public Set<CarriageAvailableAdditionalService> getAvailableAdditionalServices() {
        return availableAdditionalServices;
    }

    public void setAvailableAdditionalServices(Set<CarriageAvailableAdditionalService> availableAdditionalServices) {
        this.availableAdditionalServices = availableAdditionalServices;
    }

}