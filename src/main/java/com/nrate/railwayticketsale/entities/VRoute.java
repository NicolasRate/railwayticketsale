package com.nrate.railwayticketsale.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity(name = VRoute.ENTITY_NAME)
@Immutable
@Data
@Table(name = VRoute.TABLE_NAME)
public class VRoute {
    public static final String ENTITY_NAME = "V_Route";
    public static final String TABLE_NAME = "v_route";
    public static final String COLUMN_ORDERNUMBER_NAME = "order_number";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_ARRDATE_NAME = "arr_date";
    public static final String COLUMN_ARRTIME_NAME = "arr_time";
    public static final String COLUMN_DEPDATE_NAME = "dep_date";
    public static final String COLUMN_DEPTIME_NAME = "dep_time";
    public static final String COLUMN_TRAINID_NAME = "train_id";
    public static final String COLUMN_STATIONID_NAME = "station_id";

    @EmbeddedId
    @JsonIgnore
    private VRouteId id;

    @Column(name = COLUMN_TRAINID_NAME, nullable = false, updatable = false, insertable = false)
    private Integer trainId;

    @Column(name = COLUMN_STATIONID_NAME, nullable = false, updatable = false, insertable = false)
    private Integer stationId;

    @Column(name = COLUMN_ORDERNUMBER_NAME)
    private Integer orderNumber;

    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 50)
    private String name;

    @Column(name = COLUMN_ARRDATE_NAME)
    private LocalDate arrDate;

    @Column(name = COLUMN_ARRTIME_NAME)
    private LocalTime arrTime;

    @Column(name = COLUMN_DEPDATE_NAME)
    private LocalDate depDate;

    @Column(name = COLUMN_DEPTIME_NAME)
    private LocalTime depTime;

    public Integer getStationId() {
        return stationId;
    }

    public Integer getTrainId() {
        return trainId;
    }

    public VRouteId getId() {
        return id;
    }

    public void setId(VRouteId id) {
        this.id = id;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public String getName() {
        return name;
    }

    public LocalDate getArrDate() {
        return arrDate;
    }

    public LocalTime getArrTime() {
        return arrTime;
    }

    public LocalDate getDepDate() {
        return depDate;
    }

    public LocalTime getDepTime() {
        return depTime;
    }


    protected VRoute() {
    }
}