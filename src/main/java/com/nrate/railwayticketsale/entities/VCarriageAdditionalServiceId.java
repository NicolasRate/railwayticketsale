package com.nrate.railwayticketsale.entities;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class VCarriageAdditionalServiceId implements Serializable {
    public static final String COLUMN_CARRIAGEID_NAME = "carriage_id";
    public static final String COLUMN_ADDITIONALSERVICEID_NAME = "additional_service_id";
    private static final long serialVersionUID = -4542993020327679726L;

    @Column(name = COLUMN_CARRIAGEID_NAME, nullable = false)
    private Integer carriageId;

    @Column(name = COLUMN_ADDITIONALSERVICEID_NAME, nullable = false)
    private Integer additionalServiceId;

    public Integer getCarriageId() {
        return carriageId;
    }

    public Integer getAdditionalServiceId() {
        return additionalServiceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        VCarriageAdditionalServiceId entity = (VCarriageAdditionalServiceId) o;
        return Objects.equals(this.carriageId, entity.carriageId) &&
                Objects.equals(this.additionalServiceId, entity.additionalServiceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carriageId, additionalServiceId);
    }

}