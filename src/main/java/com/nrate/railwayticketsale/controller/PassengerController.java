package com.nrate.railwayticketsale.controller;

import com.nrate.railwayticketsale.entities.Passenger;
import com.nrate.railwayticketsale.repo.PassengerRepo;
import com.nrate.railwayticketsale.service.PassengerService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Data
@RestController
@Slf4j
@ControllerAdvice
@RequestMapping("passengers")
public class PassengerController {

    private final PassengerRepo passengerRepo;
    private final PassengerService passengerService;
    @PersistenceContext
    private final EntityManager entityManager;

    @PostMapping
    public Integer addPassenger(@RequestBody Passenger passenger) {
        Optional<Integer> id = passengerService.addPassenger(passenger);
        if(id.isEmpty()) {
            log.info(String.format("Passenger with id %d is not found", passenger.getId()));
            throw new RuntimeException(String.format("Passenger with id %d is not found", passenger.getId()));
        } else {
            log.info("Added passenger id: " + id.get());
            return id.get();
        }
    }

    @GetMapping
    public List<Passenger> getPassengers() {
        List<Passenger> passengers = passengerService.getPassengers();
        log.info(String.format("Get %d count of passengers", passengers.size()));
        return passengers;
    }

    @Transactional
    @PutMapping
    public void updatePassenger(@RequestBody Passenger passenger) {
        passengerService.updatePassenger(passenger);
        log.info("Updated passenger: " + passenger);
    }

    @GetMapping("/{id}")
    public Passenger getPassengerById(@PathVariable Integer id)  {
        Optional<Passenger> passenger = passengerService.getPassengerById(id);
        if (passenger.isPresent()) {
            Passenger passenger1 = passenger.get();
            passenger1.getTickets().forEach(ticket ->  {
                ticket.getTrain().setCarriages(null);
                ticket.getTrain().setTicketPrices(null);
            });
            log.info("Get passenger: " + passenger1.toString());
            return passenger1;
        } else {
            log.info(String.format("Passenger with id %d is not found", id));
            throw new RuntimeException(String.format("Passenger with id %d is not found", id));
        }
    }

    @DeleteMapping("/{id}")
    public void deletePassenger(@PathVariable Integer id) {
        passengerService.deletePassenger(id);
        log.info("Deleted passenger with id: " + id);
    }

    @ExceptionHandler(Throwable.class)
    public String handleError(HttpServletRequest req, RuntimeException ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);

        return "Request: " + req.getRequestURL() + " raised " + ex;
    }

}
