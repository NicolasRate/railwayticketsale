package com.nrate.railwayticketsale.controller;

import com.nrate.railwayticketsale.entities.VRoute;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@RestController
@RequestMapping("views")
@Data
@Slf4j
public class ViewsController {

    @PersistenceContext
    private final EntityManager entityManager;

    @GetMapping("/route/{trainId}")
    public List getRoutes(@PathVariable Integer trainId) {
        return entityManager.createQuery("SELECT v FROM V_Route v WHERE v.trainId = :train_id ORDER BY v.orderNumber")
                .setParameter("train_id", trainId)
                .getResultList();
    }

    @GetMapping("/carriage-additional-services/{carriageId}")
    public List getCarriageAdditionalServices(@PathVariable Integer carriageId) {
        return entityManager.createQuery("SELECT v FROM V_Carriage_Additional_Service v WHERE v.id.carriageId = :carriage_id ORDER BY v.id.additionalServiceId")
                .setParameter("carriage_id", carriageId)
                .getResultList();
    }


}
