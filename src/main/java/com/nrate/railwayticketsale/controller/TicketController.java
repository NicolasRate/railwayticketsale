package com.nrate.railwayticketsale.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nrate.railwayticketsale.entities.*;
import com.nrate.railwayticketsale.repo.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("tickets")
@Data
@Slf4j
public class TicketController {

    private final TicketRepo ticketRepo;
    private final CashierRepo cashierRepo;
    private final PassengerRepo passengerRepo;
    private final StationInRouteRepo stationInRouteRepo;
    private final CarriageRepo carriageRepo;
    private final TrainRepo trainRepo;

    @PersistenceContext
    private EntityManager entityManager;

    @PostMapping
    @Transactional
    public void addTicket(@RequestBody String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode all = mapper.readTree(json);

        StationInRoute depStationId = stationInRouteRepo.findById(StationInRouteId.builder()
                .stationId(all.get("depStationId").asInt())
                .trainId(all.get("trainId").asInt())
                .build()).get();
        log.info(depStationId.toString());
        StationInRoute arrStationId = stationInRouteRepo.findById(StationInRouteId.builder()
                .stationId(all.get("arrStationId").asInt())
                .trainId(all.get("trainId").asInt())
                .build()).get();
        log.info(arrStationId.toString());

        Train train = trainRepo.findById(all.get("trainId").asInt()).get();
        log.info(train.toString());
        Integer realDistance = arrStationId.getDistanceFromStart() - depStationId.getDistanceFromStart();
        Integer fullDistance = train.getDistance();
        BigDecimal defaultPrice = (BigDecimal)( entityManager.createQuery("SELECT tp.price FROM Ticket_Price tp WHERE tp.train.id = :train_id AND tp.placesType.id = :places_type_id")
                .setParameter("train_id", train.getId())
                .setParameter("places_type_id", all.get("placesTypeId").asInt())
                .getSingleResult());
        BigDecimal realPrice = defaultPrice.multiply(BigDecimal.valueOf((realDistance / (double) fullDistance)));

        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("CreateTicket")
                .registerStoredProcedureParameter("passenger_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("cashier_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("dep_station_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("arr_station_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("train_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("carriage_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("places_type_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("place_number", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("price", BigDecimal.class, ParameterMode.IN)
                .setParameter("passenger_id", all.get("passengerId").asInt())
                .setParameter("cashier_id", all.get("cashierId").asInt())
                .setParameter("dep_station_id", all.get("depStationId").asInt())
                .setParameter("arr_station_id", all.get("arrStationId").asInt())
                .setParameter("train_id", all.get("trainId").asInt())
                .setParameter("carriage_id", all.get("carriageId").asInt())
                .setParameter("places_type_id", all.get("placesTypeId").asInt())
                .setParameter("place_number", all.get("placeNumber").asInt())
                .setParameter("price", realPrice);

        storedProcedureQuery.execute();
        log.info("Added ticket");
    }

    @GetMapping
    public List<Ticket> getTickets() {
        List<Ticket> tickets = ticketRepo.findAll();
        log.info(String.format("Get %d count of tickets", tickets.size()));
        return tickets;
    }

}
