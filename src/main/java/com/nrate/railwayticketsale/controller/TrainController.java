package com.nrate.railwayticketsale.controller;

import com.nrate.railwayticketsale.entities.*;
import com.nrate.railwayticketsale.repo.CarriageRepo;
import com.nrate.railwayticketsale.repo.StationInRouteRepo;
import com.nrate.railwayticketsale.repo.StationRepo;
import com.nrate.railwayticketsale.repo.TrainRepo;
import com.nrate.railwayticketsale.service.TrainService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("trains")
@Data
@Slf4j
public class TrainController {

    private final TrainService trainService;
    private final StationRepo stationRepo;

    @PersistenceContext
    private EntityManager entityManager;

    @PostMapping
    @Transactional
    public List getTrains(@RequestBody Map<String, Object> body) {
        return trainService.getTrains(body);
    }

    @PostMapping("/ticketPrices")
    @Transactional
    public List getTicketPrices(@RequestBody Map<String, Object> body) {
        return trainService.getTicketPrices(body);
    }

    @GetMapping("/{id}")
    public Train getTrainById(@PathVariable Integer id) {
        return trainService.getTrainById(id);
    }

}
