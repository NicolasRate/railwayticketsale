package com.nrate.railwayticketsale.controller;

import com.nrate.railwayticketsale.entities.Station;
import com.nrate.railwayticketsale.repo.StationRepo;
import com.nrate.railwayticketsale.service.StationService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("stations")
@Data
@Slf4j
public class StationController {

    private final StationRepo stationRepo;
    private final StationService stationService;
    @PersistenceContext
    private final EntityManager entityManager;

    @PostMapping
    public Integer addStation(@RequestBody Station station) {
        Optional<Integer> id = stationService.addStation(station);
        if(id.isEmpty()) {
            log.info(String.format("Station with id %d is not found", station.getId()));
            throw new RuntimeException(String.format("Station with id %d is not found", station.getId()));
        } else {
            log.info("Added station id: " + id.get());
            return id.get();
        }
    }

    @GetMapping
    public List<Station> getStations() {
        List<Station> stations = stationService.getStations();
        log.info(String.format("Get %d count of stations", stations.size()));
        return stations;
    }

    @Transactional
    @PutMapping
    public void updateStation(@RequestBody Station station) {
        stationService.updateStation(station);
        log.info("Updated station: " + station);
    }

    @GetMapping("/{id}")
    public Station getStationById(@PathVariable Integer id)  {
        Optional<Station> station = stationService.getStationById(id);
        if (station.isPresent()) {
            Station station1 = station.get();
            log.info("Get station1: " + station1.toString());
            return station1;
        } else {
            log.info(String.format("Station with id %d is not found", id));
            throw new RuntimeException(String.format("Station with id %d is not found", id));
        }
    }

    @DeleteMapping("/{id}")
    public void deleteStation(@PathVariable Integer id) {
        stationService.deleteStation(id);
        log.info("Deleted passenger with id: " + id);
    }

    @ExceptionHandler(Throwable.class)
    public String handleError(HttpServletRequest req, RuntimeException ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);

        return "Request: " + req.getRequestURL() + " raised " + ex;
    }


}
