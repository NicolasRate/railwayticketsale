package com.nrate.railwayticketsale.repo;

import com.nrate.railwayticketsale.entities.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PassengerRepo extends JpaRepository<Passenger, Integer> {

    Optional<Passenger> findByFirstNameAndLastNameAndAge(String firstName, String lastName, Integer age);

}
