package com.nrate.railwayticketsale.repo;

import com.nrate.railwayticketsale.entities.Station;
import com.nrate.railwayticketsale.entities.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface TrainRepo extends JpaRepository<Train, Integer> {

    List<Train> findAllByStartStationAndEndStation(Station startStation, Station endStation);

    @Procedure("GetTrainByStationsAndDate")
    List<Train> getTrainByStationsAndDate(@Param("dep_station_id") Integer depStationId,
                                           @Param("arr_station_id") Integer arrStationId,
                                           @Param("dep_date") String depDate);

    @Procedure("GetTicketPrices")
    List<Object[]> getTicketPrices(@Param("dep_station_id") Integer depStationId,
                                         @Param("arr_station_id") Integer arrStationId,
                                         @Param("dep_date") String depDate);
}
