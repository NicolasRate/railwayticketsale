package com.nrate.railwayticketsale.repo;

import com.nrate.railwayticketsale.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface TicketRepo extends JpaRepository<Ticket, Integer> {

    @Procedure("CreateTicket")
    List<Ticket> addTicket(
            @Param("passenger_id") Integer passengerId,
            @Param("cashier_id") Integer cashierId,
            @Param("dep_station_id") Integer depStationId,
            @Param("arr_station_id") Integer arrStationId,
            @Param("train_id") Integer trainId,
            @Param("carriage_id") Integer carriageId,
            @Param("places_type_id") Integer placesTypeId,
            @Param("place_number") Integer placeNumber,
            @Param("price") BigDecimal price
    );

}
