package com.nrate.railwayticketsale.repo;

import com.nrate.railwayticketsale.entities.Station;
import com.nrate.railwayticketsale.entities.StationInRoute;
import com.nrate.railwayticketsale.entities.StationInRouteId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StationRepo extends JpaRepository<Station, Integer> {

    Optional<Station> findByName(String name);

}
