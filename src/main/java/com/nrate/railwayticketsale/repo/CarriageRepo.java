package com.nrate.railwayticketsale.repo;

import com.nrate.railwayticketsale.entities.Carriage;
import com.nrate.railwayticketsale.entities.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarriageRepo extends JpaRepository<Carriage, Integer> {

    List<Carriage> findAllByTrain(Train train);

}
