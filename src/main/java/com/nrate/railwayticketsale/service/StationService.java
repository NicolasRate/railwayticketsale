package com.nrate.railwayticketsale.service;

import com.nrate.railwayticketsale.entities.Station;
import com.nrate.railwayticketsale.repo.StationRepo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@Data
@Slf4j
public class StationService {

    private final StationRepo stationRepo;
    @PersistenceContext
    private final EntityManager entityManager;

    public Optional<Integer> addStation(Station station) {
        return Optional.of(stationRepo.save(station).getId());
    }

    public List<Station> getStations() {
        return stationRepo.findAll();
    }

    @Transactional
    public void updateStation(Station station) {
        stationRepo.save(station);
    }

    public Optional<Station> getStationById(Integer id) {
        return stationRepo.findById(id);
    }

    public void deleteStation(Integer id) {
        stationRepo.deleteById(id);
    }
}
