package com.nrate.railwayticketsale.service;

import com.nrate.railwayticketsale.entities.Train;
import com.nrate.railwayticketsale.repo.StationInRouteRepo;
import com.nrate.railwayticketsale.repo.StationRepo;
import com.nrate.railwayticketsale.repo.TrainRepo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;

@Service
@Data
@Slf4j
public class TrainService {

    private final TrainRepo trainRepo;
    private final StationRepo stationRepo;
    private final StationInRouteRepo stationInRouteRepo;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public List getTicketPrices(Map<String, Object> body) {
        if (!body.isEmpty()) {
            return entityManager.createNativeQuery("exec GetTicketPrices @dep_station_id = :depStation, @arr_station_id = :arrStation," +
                            "@dep_date = :dep_date")
                    .setParameter("depStation", (Integer) body.get("depStation"))
                    .setParameter("arrStation", (Integer) body.get("arrStation"))
                    .setParameter("dep_date", body.get("dep_date").toString())
                    .getResultList();
        }

        return null;
    }

    @Transactional
    public List<Train> getTrains(Map<String, Object> body) {
        if (!body.isEmpty()) {
            return trainRepo.getTrainByStationsAndDate((Integer) body.get("depStation"), (Integer) body.get("arrStation"), body.get("dep_date").toString());
        }

        return trainRepo.findAll();
    }

    public Train getTrainById(Integer id) {
        return trainRepo.findById(id).get();
    }
}
