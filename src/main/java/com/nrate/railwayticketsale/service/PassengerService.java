package com.nrate.railwayticketsale.service;

import com.nrate.railwayticketsale.entities.Passenger;
import com.nrate.railwayticketsale.repo.PassengerRepo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Service
@Data
@Slf4j
public class PassengerService {

    private final PassengerRepo passengerRepo;
    @PersistenceContext
    private final EntityManager entityManager;

    public Optional<Integer> addPassenger(Passenger passenger) {
        return Optional.of(passengerRepo.save(passenger).getId());
    }

    public List<Passenger> getPassengers() {
        return passengerRepo.findAll();
    }

    @Transactional
    public void updatePassenger(Passenger passenger) {
        passengerRepo.save(passenger);
    }

    public Optional<Passenger> getPassengerById(Integer id) {
        return passengerRepo.findById(id);
    }

    public void deletePassenger(Integer id) {
        passengerRepo.deleteById(id);
    }

}
